# Austin Airbnb room position

Investigating Austin room prices and positions with data from [Airbnb](http://insideairbnb.com/get-the-data.html). The end goal is to create an interactive map of room types the room prices in Austin.

## File description
listing.csv: raw data 
<br>map_visualization.ipynb: Python code showing the map and room positions.